Cilem je vypsat vzestupne cisla od 1 do 1000?

Prvnim problemem je ze program nevypise nic, thready se nepousti.
Druhym pak je pristup ke sdilenemu prostredku _counter.

Resenim by mohlo byt neco jako:

```c#
    private static readonly object mutex = new object();

    public static void DoWork()
    {
        for (int i = 0; i < 500; i++)
        {
            lock (mutex)
            {
                _counter = _counter + 1;
                Console.WriteLine("Line: {0}", _counter);
            }
        }
    }
```

Obecne bych se ale asi zbavil i statickych metod a vytvoril klasickou tridu, kterou bych v pripade potreby jedne instance resil jako singleton.
