# Výstup

* Diagram.jpg - prvotní nástřel schématu
* createDB.sql - sql pro vygenerovani DB
* query_a1.sql - sql reseni zadani a1
* query_a2.sql - sql reseni zadani a2

## Otázky

* Jaký je význam cenu projektu pro zákazníka? (Cena za fakturační hodinu?)
* Co je nákladovost projektu? (Cena práce jednotlivých zaměstnanců?)
* Co je vytíženost zaměstnance? (Součet hodin na všech projektech?)