SELECT
	p.[Title]
	,SUM(wl.[Hours]) * p.Rate AS [Total costs]
FROM
	[WorkLog] wl WITH(NOLOCK)
JOIN 
	[Project] p WITH(NOLOCK) ON p.[Id] = wl.[ProjectId]
GROUP BY
	p.[Title], p.Rate
ORDER BY 
	[Total costs] DESC