SELECT 
	e.FullName
	,SUM(wl.[Hours]) [Total hours]
FROM
	[WorkLog] wl WITH(NOLOCK)
JOIN
	[Employee] e WITH(NOLOCK) ON e.Id = wl.EmployeeId
GROUP BY
	e.FullName
ORDER BY 
	[Total hours] ASC