﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dp.creasoft.b.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace dp.creasoft.b.web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private readonly IPersonService _personService;

        public HomeController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("person/generator/{count}")]
        public IActionResult Create(int count)
        {
            this._personService.RegeneratePersons(count);
            return RedirectToAction("List");
        }

        [HttpGet("person/list")]
        public IActionResult List()
        {
            var model = this._personService.DataContext.Persons.GetAll();
            return View(model);
        }
    }
}