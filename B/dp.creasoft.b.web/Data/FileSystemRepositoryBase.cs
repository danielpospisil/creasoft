﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using dp.creasoft.b.web.Data.DTO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace dp.creasoft.b.web.Data
{
    public abstract class FileSystemRepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : EntityBase
    {
        protected readonly IConfiguration _config;
        protected readonly IHostingEnvironment _env;
        private readonly ILogger _logger;

        private readonly List<TEntity> _dataEntities;
        
        public FileSystemRepositoryBase(IConfiguration config, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            _config = config;
            _env = env;
            _logger = loggerFactory.CreateLogger(nameof(FileSystemRepositoryBase<TEntity>));

            _dataEntities = LoadData();
        }

        private List<TEntity> LoadData()
        {
            var path = Path.Combine(_env.ContentRootPath, _config["FileSystemRepositoryPath"],
                $"{typeof(TEntity).ToString()}.json");
            var ret = new List<TEntity>();

            try
            {
                using (var sr = new StreamReader(path))
                {
                    var jsonString = sr.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(jsonString))
                    {
                        ret = JsonConvert.DeserializeObject<List<TEntity>>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
            }

            return ret;

        }

        public void Add(TEntity entity)
        {
            this._dataEntities.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            this._dataEntities.AddRange(entities);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return this._dataEntities.Where(predicate.Compile());
        }

        public TEntity Get(int id)
        {
            return this._dataEntities.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this._dataEntities;
        }

        public void Remove(TEntity entity)
        {
            this._dataEntities.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            foreach (var item in entities)
            {
                Remove(item);
            }
        }
    }
}
