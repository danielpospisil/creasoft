﻿using dp.creasoft.b.web.Data.DTO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace dp.creasoft.b.web.Data.Repositories
{
    public class PersonRepository : FileSystemRepositoryBase<Person>, IPersonRepository
    {

        public PersonRepository(IConfiguration config, IHostingEnvironment env, ILoggerFactory loggerFactory) :base(config,env, loggerFactory)
        { 
        }
    }
}
