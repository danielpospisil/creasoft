﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dp.creasoft.b.web.Data.DTO;

namespace dp.creasoft.b.web.Data.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
    }
}
