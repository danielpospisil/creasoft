﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dp.creasoft.b.web.Data.Repositories;

namespace dp.creasoft.b.web.Data
{
    public interface IUnitOfWork
    {
        IPersonRepository Persons { get; }

        int Complete();
    }
}
