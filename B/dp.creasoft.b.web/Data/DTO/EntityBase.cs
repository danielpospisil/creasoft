﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dp.creasoft.b.web.Data.DTO
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
}
