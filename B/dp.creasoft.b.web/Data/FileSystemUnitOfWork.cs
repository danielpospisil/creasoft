﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using dp.creasoft.b.web.Data.DTO;
using dp.creasoft.b.web.Data.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace dp.creasoft.b.web.Data
{
    public class FileSystemUnitOfWork : IUnitOfWork
    {
        private readonly IConfiguration _config;
        private readonly IHostingEnvironment _env;

        public FileSystemUnitOfWork(IConfiguration config, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            _config = config;
            _env = env;
            this.Persons = new PersonRepository(config,env, loggerFactory);
        }

        public IPersonRepository Persons { get; private set; }

        public int Complete()
        {
            Save(Persons.GetAll());

            return 1;
        }

        private void Save<TEntity>(IEnumerable<TEntity> data)
        {
            var path = Path.Combine(_env.ContentRootPath, _config["FileSystemRepositoryPath"], $"{typeof(TEntity).ToString()}.json");
            var ret = new List<TEntity>();

            using (var sw = new StreamWriter(path,false))
            {
                var jsonString = JsonConvert.SerializeObject(data);
                sw.Write(jsonString);
            }
        }
    }
}
