﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dp.creasoft.b.web.Data;
using dp.creasoft.b.web.Data.DTO;
using RandomNameGeneratorLibrary;

namespace dp.creasoft.b.web.Services
{
    public interface IPersonService
    {
        IUnitOfWork DataContext { get; }
        void RegeneratePersons(int count);
    }

    public class PersonService : IPersonService
    {
        public IUnitOfWork DataContext { get; private set; }

        public PersonService(IUnitOfWork context)
        {
            DataContext = context;
        }

        public void RegeneratePersons(int count)
        {
            var generator = new PersonNameGenerator();
            var random = new Random(DateTime.Now.Millisecond);

            this.DataContext.Persons.RemoveRange(this.DataContext.Persons.GetAll());

            for (var i = 0; i < count; i++)
            {
                var person = new Person
                {
                    FirstName = generator.GenerateRandomFirstName(),
                    LastName = generator.GenerateRandomLastName(),
                    Age = random.Next(18, 98),
                };
                this.DataContext.Persons.Add(person);
            }

            this.DataContext.Complete();
        }
    }
}
